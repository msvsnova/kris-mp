#------------------------------------------------------------------------------
#
# MP APPLICATION - MAKEFILE
# By FIST Kresimir Mandic
#
#------------------------------------------------------------------------------


CC          = start /w c:\vscd\bin\nova
CFLAGS      = -w -ereadfgl

TD			= X:\mp\ 
COMD        = .\lib\mpcommon.fle 

#TD			= T:\mp\ 

AD          = Y:\mp\ 
#------------------------------------------------------------------------------
MPFILES     = $(TD)mp.fle $(TD)mp_dd.fle $(TD)orgs.fle     $(TD)horgs.fle    \
	$(TD)porgs.fle    $(TD)ppar.fle      $(TD)vr_ppar.fle  $(TD)zemlje.fle   \
	$(TD)valute.fle   $(TD)ispporup.fle  $(TD)lokacije.fle $(TD)opcine.fle   \
	$(TD)orgs_lok.fle $(TD)ppar_adr.fle  $(TD)radnici.fle  $(TD)tecajevi.fle \
	$(TD)zupanije.fle $(TD)l_ispup.fle   $(TD)l_lok.fle    $(TD)l_opcine.fle \
	$(TD)l_orglok.fle $(TD)l_tecaj.fle   $(TD)l_valute.fle $(TD)l_zemlje.fle \
	$(TD)l_zupan.fle  $(TD)l_orgs.fle    $(TD)ppzirorn.fle $(TD)l_ppar.fle   \
	$(TD)l_radnic.fle $(TD)l_ppadr.fle   $(TD)l_ppzrn.fle  $(TD)orgs2.fle    \
	$(TD)org_org2.fle $(TD)l_horgs.fle   $(TD)r_orgs.fle   $(TD)objekti.fle  \
	$(TD)obj_new.fle  $(TD)novi_obj.fle  $(TD)l_obj.fle    $(TD)rba_orgs.fle \
	$(TD)l_nobj.fle   $(TD)fink_mp.fle   $(TD)vr_tecaj.fle $(TD)mpdbutil.fle \
    $(TD)mp_ascii.fle $(TD)lppar_dupl.fle $(TD)mp_idx.fle  $(TD)rbalokoj.fle   \
    $(TD)mp_start.fle $(TD)ppar_kont.fle $(TD)l_ppkont.fle $(TD)pparnal.fle  \
    $(TD)ppar_grp.fle $(TD)mp_rev.fle    $(TD)ppar_komit.fle $(TD)joppdprilog1.fle \
    $(TD)joppdprilog2.fle $(TD)joppdprilog3.fle $(TD)joppdprilog4.fle $(TD)joppdprilog5.fle \
    $(TD)joppdprilog6.fle $(TD)joppdprilog7.fle $(TD)joppdprilog8.fle $(TD)joppdprilog9.fle \
    $(TD)states.fle   $(TD)projekti.fle

COMMON     = lokacije.ls mp_dd.ls mp_ddx.ls opcine.ls org_org2.ls orgs.ls orgs2.ls \
             porgs.ls ppar.ls ppar_adr.ls ppar_komit.ls ppzirorn.ls radnici.ls     \
             tecajevi.ls valute.ls vr_tecaj.ls zemlje.ls orgs_lok.ls objekti.ls    \
             vr_ppar.ls ppar_grp.ls



LOCCO      = $(TD)lo_dd.fle   $(TD)lo_dest.fle $(TD)lo_rute.fle \
             $(TD)locco.fle   $(TD)lo_cijene30.fle 

all         : $(MPFILES) 

##$(LOCCO)

$(TD)lo_dd.fle:  lo_dd.lgc
    $(CC) $(CFLAGS) appl=lo_dd dotfle=$(TD)lo_dd.fle

$(TD)lo_dest.fle:  lo_dest.lgc
    $(CC) $(CFLAGS) appl=lo_dest dotfle=$(TD)lo_dest.fle

$(TD)lo_rute.fle:  lo_rute.lgc
    $(CC) $(CFLAGS) appl=lo_rute dotfle=$(TD)lo_rute.fle

$(TD)lo_cijene30.fle:  lo_cijene30.lgc
    $(CC) $(CFLAGS) appl=lo_cijene30 dotfle=$(TD)lo_cijene30.fle

$(TD)locco.fle:  locco.lgc
    $(CC) $(CFLAGS) appl=locco dotfle=$(TD)locco.fle

##COMMON

lokacije.ls:  lokacije.lgc
    $(CC) $(CFLAGS) appl=lokacije dotfle=$(COMD)
    @echo " ">lokacije.ls

mp_dd.ls:  mp_dd.lgc
    $(CC) $(CFLAGS) appl=mp_dd dotfle=.\lib\mp_dd.fle 
    @echo " ">mp_dd.ls

mp_ddx.ls:  mp_ddx.lgc
    $(CC) $(CFLAGS) appl=mp_ddx dotfle=.\lib\mp_dd.fle 
    @echo " ">mp_ddx.ls

opcine.ls:  opcine.lgc
    $(CC) $(CFLAGS) appl=opcine dotfle=$(COMD)
    @echo " ">opcine.ls

org_org2.ls:  org_org2.lgc
    $(CC) $(CFLAGS) appl=org_org2 dotfle=$(COMD)
    @echo " ">org_org2.ls

orgs.ls:  orgs.lgc
    $(CC) $(CFLAGS) appl=orgs dotfle=$(COMD)
    @echo " ">orgs.ls

orgs2.ls:  orgs2.lgc
    $(CC) $(CFLAGS) appl=orgs2 dotfle=$(COMD)
    @echo " ">orgs2.ls

porgs.ls:  porgs.lgc
    $(CC) $(CFLAGS) appl=porgs dotfle=$(COMD)
    @echo " ">porgs.ls

ppar.ls:  ppar.lgc
    $(CC) $(CFLAGS) appl=ppar dotfle=$(COMD)
    @echo " ">ppar.ls

ppar_adr.ls:  ppar_adr.lgc
    $(CC) $(CFLAGS) appl=ppar_adr dotfle=$(COMD)
    @echo " ">ppar_adr.ls

ppar_komit.ls:  ppar_komit.lgc
    $(CC) $(CFLAGS) appl=ppar_komit dotfle=$(COMD)
    @echo " ">ppar_komit.ls

ppzirorn.ls:  ppzirorn.lgc
    $(CC) $(CFLAGS) appl=ppzirorn dotfle=$(COMD)
    @echo " ">ppzirorn.ls

radnici.ls:  radnici.lgc
    $(CC) $(CFLAGS) appl=radnici dotfle=$(COMD)
    @echo " ">radnici.ls

tecajevi.ls:  tecajevi.lgc
    $(CC) $(CFLAGS) appl=tecajevi dotfle=$(COMD)
    @echo " ">tecajevi.ls

vr_ppar.ls:  vr_ppar.lgc
    $(CC) $(CFLAGS) appl=vr_ppar dotfle=$(COMD)
    @echo " ">vr_ppar.ls

ppar_grp.ls:  ppar_grp.lgc
    $(CC) $(CFLAGS) appl=ppar_grp dotfle=$(COMD)
    @echo " ">ppar_grp.ls

valute.ls:  valute.lgc
    $(CC) $(CFLAGS) appl=valute dotfle=$(COMD)
    @echo " ">valute.ls

vr_tecaj.ls:  vr_tecaj.lgc
    $(CC) $(CFLAGS) appl=vr_tecaj dotfle=$(COMD)
    @echo " ">vr_tecaj.ls

zemlje.ls:  zemlje.lgc
    $(CC) $(CFLAGS) appl=zemlje dotfle=$(COMD)
    @echo " ">zemlje.ls

orgs_lok.ls:  orgs_lok.lgc
    $(CC) $(CFLAGS) appl=orgs_lok dotfle=$(COMD)
    @echo " ">orgs_lok.ls

objekti.ls:  objekti.lgc
    $(CC) $(CFLAGS) appl=objekti dotfle=$(COMD)
    @echo " ">objekti.ls

########################################

$(TD)mp_dd.fle:  mp_dd.lgc
    $(CC) $(CFLAGS) appl=mp_dd dotfle=$(TD)mp_dd.fle
    $(CC) -w genddx ddfile=mp_dd
    $(CC) $(CFLAGS) appl=mp_ddx dotfle=$(TD)mp_ddx.fle
    

$(TD)mp.fle:  mp.lgc
    $(CC) $(CFLAGS) appl=mp dotfle=$(TD)mp.fle

$(TD)horgs.fle:  horgs.lgc
    $(CC) $(CFLAGS) appl=horgs dotfle=$(TD)horgs.fle

$(TD)orgs.fle:  orgs.lgc
    $(CC) $(CFLAGS) appl=orgs dotfle=$(TD)orgs.fle

$(TD)porgs.fle:  porgs.lgc
    $(CC) $(CFLAGS) appl=porgs dotfle=$(TD)porgs.fle

$(TD)ppar.fle:  ppar.lgc
    $(CC) $(CFLAGS) appl=ppar dotfle=$(TD)ppar.fle

$(TD)vr_ppar.fle:  vr_ppar.lgc
    $(CC) $(CFLAGS) appl=vr_ppar dotfle=$(TD)vr_ppar.fle

$(TD)zemlje.fle:  zemlje.lgc
    $(CC) $(CFLAGS) appl=zemlje dotfle=$(TD)zemlje.fle

$(TD)valute.fle:  valute.lgc
    $(CC) $(CFLAGS) appl=valute dotfle=$(TD)valute.fle

$(TD)ispporup.fle:  ispporup.lgc
    $(CC) $(CFLAGS) appl=ispporup dotfle=$(TD)ispporup.fle

$(TD)lokacije.fle:  lokacije.lgc
    $(CC) $(CFLAGS) appl=lokacije dotfle=$(TD)lokacije.fle

$(TD)opcine.fle:  opcine.lgc
    $(CC) $(CFLAGS) appl=opcine dotfle=$(TD)opcine.fle

$(TD)orgs_lok.fle:  orgs_lok.lgc
    $(CC) $(CFLAGS) appl=orgs_lok dotfle=$(TD)orgs_lok.fle

$(TD)ppar_adr.fle:  ppar_adr.lgc
    $(CC) $(CFLAGS) appl=ppar_adr dotfle=$(TD)ppar_adr.fle

$(TD)ppar_kont.fle:  ppar_kont.lgc
    $(CC) $(CFLAGS) appl=ppar_kont dotfle=$(TD)ppar_kont.fle

$(TD)radnici.fle:  radnici.lgc
    $(CC) $(CFLAGS) appl=radnici dotfle=$(TD)radnici.fle

$(TD)tecajevi.fle:  tecajevi.lgc
    $(CC) $(CFLAGS) appl=tecajevi dotfle=$(TD)tecajevi.fle

$(TD)zupanije.fle:  zupanije.lgc
    $(CC) $(CFLAGS) appl=zupanije dotfle=$(TD)zupanije.fle

$(TD)l_ispup.fle:  l_ispup.lgc
    $(CC) $(CFLAGS) appl=l_ispup dotfle=$(TD)l_ispup.fle

$(TD)l_lok.fle:  l_lok.lgc
    $(CC) $(CFLAGS) appl=l_lok dotfle=$(TD)l_lok.fle

$(TD)l_opcine.fle:  l_opcine.lgc
    $(CC) $(CFLAGS) appl=l_opcine dotfle=$(TD)l_opcine.fle

$(TD)l_orglok.fle:  l_orglok.lgc
    $(CC) $(CFLAGS) appl=l_orglok dotfle=$(TD)l_orglok.fle

$(TD)l_tecaj.fle:  l_tecaj.lgc
    $(CC) $(CFLAGS) appl=l_tecaj dotfle=$(TD)l_tecaj.fle

$(TD)l_valute.fle:  l_valute.lgc
    $(CC) $(CFLAGS) appl=l_valute dotfle=$(TD)l_valute.fle

$(TD)l_zemlje.fle:  l_zemlje.lgc
    $(CC) $(CFLAGS) appl=l_zemlje dotfle=$(TD)l_zemlje.fle

$(TD)l_zupan.fle:  l_zupan.lgc
    $(CC) $(CFLAGS) appl=l_zupan dotfle=$(TD)l_zupan.fle

$(TD)l_orgs.fle:  l_orgs.lgc
    $(CC) $(CFLAGS) appl=l_orgs dotfle=$(TD)l_orgs.fle

$(TD)ppzirorn.fle:  ppzirorn.lgc
    $(CC) $(CFLAGS) appl=ppzirorn dotfle=$(TD)ppzirorn.fle

$(TD)l_ppar.fle:  l_ppar.lgc
    $(CC) $(CFLAGS) appl=l_ppar dotfle=$(TD)l_ppar.fle

$(TD)l_radnic.fle:  l_radnic.lgc
    $(CC) $(CFLAGS) appl=l_radnic dotfle=$(TD)l_radnic.fle

$(TD)l_ppadr.fle:  l_ppadr.lgc
    $(CC) $(CFLAGS) appl=l_ppadr dotfle=$(TD)l_ppadr.fle

$(TD)l_ppkont.fle:  l_ppkont.lgc
    $(CC) $(CFLAGS) appl=l_ppkont dotfle=$(TD)l_ppkont.fle

$(TD)pparnal.fle:  pparnal.lgc
    $(CC) $(CFLAGS) appl=pparnal dotfle=$(TD)pparnal.fle

$(TD)l_ppzrn.fle:  l_ppzrn.lgc
    $(CC) $(CFLAGS) appl=l_ppzrn dotfle=$(TD)l_ppzrn.fle

$(TD)orgs2.fle:  orgs2.lgc
    $(CC) $(CFLAGS) appl=orgs2 dotfle=$(TD)orgs2.fle

$(TD)org_org2.fle:  org_org2.lgc
    $(CC) $(CFLAGS) appl=org_org2 dotfle=$(TD)org_org2.fle

$(TD)l_horgs.fle:  l_horgs.lgc
    $(CC) $(CFLAGS) appl=l_horgs dotfle=$(TD)l_horgs.fle

$(TD)r_orgs.fle:  r_orgs.lgc
    $(CC) $(CFLAGS) appl=r_orgs dotfle=$(TD)r_orgs.fle

$(TD)objekti.fle:  objekti.lgc
    $(CC) $(CFLAGS) appl=objekti dotfle=$(TD)objekti.fle

$(TD)obj_new.fle:  obj_new.lgc
    $(CC) $(CFLAGS) appl=obj_new dotfle=$(TD)obj_new.fle

$(TD)novi_obj.fle:  novi_obj.lgc
    $(CC) $(CFLAGS) appl=novi_obj dotfle=$(TD)novi_obj.fle

$(TD)l_obj.fle:  l_obj.lgc
    $(CC) $(CFLAGS) appl=l_obj dotfle=$(TD)l_obj.fle

$(TD)l_nobj.fle:  l_nobj.lgc
    $(CC) $(CFLAGS) appl=l_nobj dotfle=$(TD)l_nobj.fle

$(TD)fink_mp.fle:  fink_mp.lgc
    $(CC) $(CFLAGS) appl=fink_mp dotfle=$(TD)fink_mp.fle

$(TD)vr_tecaj.fle:  vr_tecaj.lgc
    $(CC) $(CFLAGS) appl=vr_tecaj dotfle=$(TD)vr_tecaj.fle

$(TD)mp_ascii.fle:  mp_ascii.lgc
    $(CC) $(CFLAGS) appl=mp_ascii dotfle=$(TD)mp_ascii.fle

$(TD)mpdbutil.fle:  mpdbutil.lgc
    $(CC) $(CFLAGS) appl=mpdbutil dotfle=$(TD)mpdbutil.fle
    $(CC) $(CFLAGS) appl=mpdbutil dotfle=$(AD)mpdbutil.fle

$(TD)lppar_dupl.fle:  lppar_dupl.lgc
    $(CC) $(CFLAGS) appl=lppar_dupl dotfle=$(TD)lppar_dupl.fle

$(TD)mp_idx.fle:  mp_idx.lgc
    $(CC) $(CFLAGS) appl=mp_idx dotfle=$(TD)mp_idx.fle

$(TD)rba_orgs.fle:  rba_orgs.lgc
    $(CC) $(CFLAGS) appl=rba_orgs dotfle=$(TD)rba_orgs.fle

$(TD)rbalokoj.fle:  rbalokoj.lgc
    $(CC) $(CFLAGS) appl=rbalokoj dotfle=$(TD)rbalokoj.fle

$(TD)mp_start.fle:  mp_start.lgc
    $(CC) $(CFLAGS) appl=mp_start dotfle=$(TD)mp_start.fle

$(TD)ppar_grp.fle:  ppar_grp.lgc
    $(CC) $(CFLAGS) appl=ppar_grp dotfle=$(TD)ppar_grp.fle

$(TD)mp_rev.fle:  mp_rev.lgc
    $(CC) $(CFLAGS) appl=mp_rev dotfle=$(TD)mp_rev.fle

$(TD)ppar_komit.fle:  ppar_komit.lgc
    $(CC) $(CFLAGS) appl=ppar_komit dotfle=$(TD)ppar_komit.fle

$(TD)joppdprilog1.fle:  joppdprilog1.lgc
    $(CC) $(CFLAGS) appl=joppdprilog1 dotfle=$(TD)joppdprilog1.fle

$(TD)joppdprilog2.fle:  joppdprilog2.lgc
    $(CC) $(CFLAGS) appl=joppdprilog2 dotfle=$(TD)joppdprilog2.fle

$(TD)joppdprilog3.fle:  joppdprilog3.lgc
    $(CC) $(CFLAGS) appl=joppdprilog3 dotfle=$(TD)joppdprilog3.fle

$(TD)joppdprilog4.fle:  joppdprilog4.lgc
    $(CC) $(CFLAGS) appl=joppdprilog4 dotfle=$(TD)joppdprilog4.fle

$(TD)joppdprilog5.fle:  joppdprilog5.lgc
    $(CC) $(CFLAGS) appl=joppdprilog5 dotfle=$(TD)joppdprilog5.fle

$(TD)joppdprilog6.fle:  joppdprilog6.lgc
    $(CC) $(CFLAGS) appl=joppdprilog6 dotfle=$(TD)joppdprilog6.fle

$(TD)joppdprilog7.fle:  joppdprilog7.lgc
    $(CC) $(CFLAGS) appl=joppdprilog7 dotfle=$(TD)joppdprilog7.fle

$(TD)joppdprilog8.fle:  joppdprilog8.lgc
    $(CC) $(CFLAGS) appl=joppdprilog8 dotfle=$(TD)joppdprilog8.fle

$(TD)joppdprilog9.fle:  joppdprilog9.lgc
    $(CC) $(CFLAGS) appl=joppdprilog9 dotfle=$(TD)joppdprilog9.fle

$(TD)states.fle:  states.lgc
    $(CC) $(CFLAGS) appl=states dotfle=$(TD)states.fle

$(TD)projekti.fle:  projekti.lgc
    $(CC) $(CFLAGS) appl=projekti dotfle=$(TD)projekti.fle

