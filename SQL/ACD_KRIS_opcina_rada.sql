ALTER TABLE radnici ADD opcina_rada VARCHAR(20) NULL
go
ALTER TABLE radnici ADD opcina_stanovanja VARCHAR(20) NULL
go
alter table iiradn add iimin_osn_dop decimal(15,2) NULL
go
update iiradn set iimin_osn_dop=0
go

INSERT INTO postavke VALUES('OPC_RAD_STAN','Op�ina rada','D')
go

alter table states add eu_pripadnost char(1) NULL
go
update states set eu_pripadnost='N'
go
update states set eu_pripadnost = zemlje.eu_pripadnost
from states join zemlje on (states.iso_ozn2 = zemlje.iso_ozn2)
where zemlje.eu_pripadnost='D'
go
drop table ah_aaradn
