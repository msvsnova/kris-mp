alter table orgs alter column naz_org char(100)
go
alter table orgs2 alter column naz_org2 char(100)
go
alter table radnici alter column rad_ime char(20)
go
alter table epdv_mp alter column epdv_ime char(20)
go
alter table ahzregos alter column rad_ime char(20)
go
alter table aazregos alter column rad_ime char(20)
go
alter table plM4 alter column rad_ime char(20)
go
alter table iiradn alter column rad_imepre char(50)
go
alter table sk_default add def_por_ok_ne char(2) NULL
go
update sk_default set def_por_ok_ne='OK'
go
alter table uzracuni add ind_ppom char(1) NULL
go
update uzracuni set ind_ppom='N'
go
