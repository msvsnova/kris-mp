alter table states add eu_pripadnost char(1) NULL
go
update states set eu_pripadnost='N'
go
update states set eu_pripadnost = zemlje.eu_pripadnost
from states join zemlje on (states.iso_ozn2 = zemlje.iso_ozn2)
where zemlje.eu_pripadnost='D'
