alter table fak_ppar alter column adr_ppar varchar(200) NOT NULL
go
alter table fak_ppartmp alter column adr_ppar varchar(200) NOT NULL
go
alter table fakprint alter column adr_ppar varchar(200) NOT NULL
go
alter table ppar alter column adr_ppar varchar(200) NOT NULL
go
alter table ppar_log alter column adr_ppar varchar(200) NOT NULL
go

alter table fak_ppar alter column oib varchar(50) NULL
go
alter table fakprint alter column v_lfakt_ppar_oib varchar(50) NULL
go
alter table fakturisti alter column fisk_oib varchar(50) NULL
go
alter table hsc_nazfak alter column oib varchar(50) NULL
go
alter table ppar alter column oib varchar(50) NULL
go
alter table pl_id_mp alter column id_oib varchar(50) NULL
go
alter table opz_stat alter column oib varchar(50) NULL
go
alter table ppo_zbirno alter column ppog_oib varchar(50) NULL
go
